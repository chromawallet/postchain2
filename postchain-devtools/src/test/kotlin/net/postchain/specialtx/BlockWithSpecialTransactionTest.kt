// Copyright (c) 2020 ChromaWay AB. See README for license information.

package net.postchain.specialtx

import net.postchain.base.SECP256K1CryptoSystem
import net.postchain.core.BlockchainRid
import net.postchain.devtools.IntegrationTestSetup
import net.postchain.devtools.testinfra.TestOneOpGtxTransaction
import net.postchain.gtx.GTXAutoSpecialTxExtension
import net.postchain.gtx.GTXOperation
import net.postchain.gtx.GTXTransaction
import net.postchain.gtx.GTXTransactionFactory
import org.junit.Assert
import org.junit.Test

/**
 * Produces blocks containing Special transactions using the simplest possible setup, but as a minimub we need a new
 * custom test module to give us the "__xxx" operations needed.
 */
class BlockWithSpecialTransactionTest : IntegrationTestSetup() {

    private lateinit var gtxTxFactory: GTXTransactionFactory

    val chainId = 1L // We only have one.

    private fun tx(id: Int): GTXTransaction {
        return TestOneOpGtxTransaction(gtxTxFactory, id, "gtx_empty", arrayOf()).getGTXTransaction()
    }

    private fun txBegin(id: Int): GTXTransaction {
        return TestOneOpGtxTransaction(
            gtxTxFactory,
            id,
            GTXAutoSpecialTxExtension.OP_BEGIN_BLOCK,
            arrayOf()
        ).getGTXTransaction()
    }

    private fun txEnd(id: Int): GTXTransaction {
        return TestOneOpGtxTransaction(
            gtxTxFactory,
            id,
            GTXAutoSpecialTxExtension.OP_END_BLOCK,
            arrayOf()
        ).getGTXTransaction()
    }

    @Test(timeout = 2 * 60 * 1000L)
    fun testBlockContent() {
        val count = 3
        configOverrides.setProperty("testpeerinfos", createPeerInfos(count))
        configOverrides.setProperty("api.port", 0)
        createNodes(count, "/net/postchain/devtools/specialtx/blockchain_config_3node_gtx.xml")

        // --------------------
        // Needed to create TXs
        // --------------------
        val blockchainRID: BlockchainRid = nodes[0].getBlockchainInstance().getEngine().getConfiguration().blockchainRid
        val module = SpecialTxTestGTXModule() // Had to build a special module for this test
        val cs = SECP256K1CryptoSystem()
        gtxTxFactory = GTXTransactionFactory(blockchainRID, module, cs)

        // --------------------
        // Create TXs
        // --------------------
        var currentHeight = 0L
        buildBlockNoWait(
            nodes, chainId, currentHeight,
            txBegin(0), // We MUST start with a special begin TX (or we will get an exception)
            tx(1),
            txEnd(2) // We MUST end with a special TX
        )
        awaitHeight(chainId, currentHeight)

        currentHeight++
        buildBlockNoWait(
            nodes, chainId, currentHeight,
            txBegin(3),
            tx(4),
            txEnd(5)
        )
        awaitHeight(chainId, currentHeight)

        currentHeight++
        buildBlockNoWait(
            nodes, chainId, currentHeight,
            txBegin(6),
            tx(7),
            txEnd(8)
        )
        awaitHeight(chainId, currentHeight)

        // --------------------
        // Actual test
        // --------------------
        val expectedNumberOfTxs = 3

        val bockQueries = nodes[0].getBlockchainInstance().getEngine().getBlockQueries()
        for (i in 0..2) {

            val blockData = bockQueries.getBlockAtHeight(i.toLong()).get()!!
            //System.out.println("block $i fetched.")

            Assert.assertEquals(expectedNumberOfTxs, blockData.transactions.size)

            checkForBegin(blockData.transactions[0])
            checkForTx(blockData.transactions[1])
            checkForEnd(blockData.transactions[2])
        }
    }

    private fun checkForBegin(tx: ByteArray) {
        checkForOperation(tx, GTXAutoSpecialTxExtension.OP_BEGIN_BLOCK)
    }

    private fun checkForTx(tx: ByteArray) {
        checkForOperation(tx, "gtx_empty")
    }

    private fun checkForEnd(tx: ByteArray) {
        checkForOperation(tx, GTXAutoSpecialTxExtension.OP_END_BLOCK)
    }

    private fun checkForOperation(tx: ByteArray, opName: String) {
        val txGtx = gtxTxFactory.decodeTransaction(tx) as GTXTransaction
        Assert.assertEquals(1, txGtx.ops.size)
        val op = txGtx.ops[0] as GTXOperation
        //System.out.println("Op : ${op.toString()}")
        Assert.assertEquals(opName, op.data.opName)
    }
}