<!-- Copyright (c) 2020 ChromaWay AB. See README for license information. -->

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>net.postchain</groupId>
    <artifactId>postchain</artifactId>
    <version>3.4.1</version>
    <modules>
        <module>postchain-common</module>
        <module>postchain-base</module>
        <module>postchain-devtools</module>
        <module>postchain-cw-modules</module>
        <module>postchain-docker</module>
        <module>postchain-client</module>
        <module>postchain-distribution</module>
    </modules>
    <packaging>pom</packaging>

    <name>Postchain</name>
    <url>http://postchain.org</url>
    <properties>
        <doclint>none</doclint>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>

        <!-- Kotlin -->
        <kotlin.compiler.incremental>false</kotlin.compiler.incremental>
        <kotlin.version>1.4.30</kotlin.version>
        <kotlin.compiler.jvmTarget>11</kotlin.compiler.jvmTarget>

        <!-- Maven -->
        <maven.source.plugin.version>3.0.1</maven.source.plugin.version>
        <maven.surefire.plugin.version>2.22.1</maven.surefire.plugin.version>
        <maven.compiler.plugin.version>3.5.1</maven.compiler.plugin.version>
        <maven.assembly.plugin.version>3.2.0</maven.assembly.plugin.version>

        <!-- Testing -->
        <junit.version>4.12</junit.version>
        <junitparams.version>1.1.1</junitparams.version>
        <mockito-kotlin.version>4.0.0</mockito-kotlin.version>
        <mockito.version>2.23.0</mockito.version>
        <assertk.version>0.10</assertk.version>
        <awaitility.version>3.1.2</awaitility.version>

        <!-- Logging -->
        <kotlin-logging.version>1.12.5</kotlin-logging.version>
        <slf4j.version>1.7.32</slf4j.version>
        <log4j2.version>2.16.0</log4j2.version>
        <jackson.version>2.13.0</jackson.version>

        <!-- 3rd party -->
        <httpclient.version>4.5.3</httpclient.version>
        <json.version>20170516</json.version>
        <kovenant-core.version>3.3.0</kovenant-core.version>
        <gson.version>2.8.5</gson.version>
        <jgitflow.maven.plugin.version>1.0-m5.1</jgitflow.maven.plugin.version>

        <maven.compiler.source>11</maven.compiler.source>
        <maven.compiler.target>11</maven.compiler.target>
    </properties>

    <repositories>
        <repository>
            <id>ProjectRepo</id>
            <name>ProjectRepo</name>
            <url>file://${project.basedir}/lib</url>
        </repository>
        <repository>
            <id>jcentral</id>
            <name>bintray</name>
            <url>https://jcenter.bintray.com</url>
        </repository>
    </repositories>

    <!-- Global dependencies -->
    <dependencies>
        <!-- Testing -->
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <scope>test</scope>
        </dependency>

        <!-- Logging -->
        <dependency>
            <groupId>org.apache.logging.log4j</groupId>
            <artifactId>log4j-core</artifactId>
            <version>${log4j2.version}</version>
        </dependency>
        <dependency>
            <groupId>org.apache.logging.log4j</groupId>
            <artifactId>log4j-api</artifactId>
            <version>${log4j2.version}</version>
        </dependency>
        <dependency>
            <groupId>org.apache.logging.log4j</groupId>
            <artifactId>log4j-slf4j-impl</artifactId>
            <version>${log4j2.version}</version>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
            <version>${slf4j.version}</version>
        </dependency>
        <!-- For Log4j2/YAML configuration -->
        <dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-databind</artifactId>
            <version>${jackson.version}</version>
        </dependency>
        <dependency>
            <groupId>com.fasterxml.jackson.dataformat</groupId>
            <artifactId>jackson-dataformat-yaml</artifactId>
            <version>${jackson.version}</version>
        </dependency>
        <dependency>
            <groupId>io.github.microutils</groupId>
            <artifactId>kotlin-logging</artifactId>
            <version>${kotlin-logging.version}</version>
        </dependency>
    </dependencies>

    <dependencyManagement>
        <dependencies>
            <!-- Kotlin -->
            <dependency>
                <groupId>org.jetbrains.kotlin</groupId>
                <artifactId>kotlin-stdlib-jdk8</artifactId> <!-- Contains extension methods for jdk8+ (Streams, autocloseable etc -->
                <version>${kotlin.version}</version>
            </dependency>
            <dependency>
                <groupId>org.jetbrains.kotlin</groupId>
                <artifactId>kotlin-reflect</artifactId>
                <version>${kotlin.version}</version>
            </dependency>
            <dependency>
                <groupId>org.jetbrains.kotlin</groupId>
                <artifactId>kotlin-test</artifactId>
                <version>${kotlin.version}</version>
                <scope>test</scope>
            </dependency>
            <dependency>
                <groupId>org.jetbrains.kotlin</groupId>
                <artifactId>kotlin-test-junit</artifactId>
                <version>${kotlin.version}</version>
                <scope>test</scope>
            </dependency>

            <!-- Postchain -->
            <dependency>
                <groupId>net.postchain</groupId>
                <artifactId>postchain-base</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>net.postchain</groupId>
                <artifactId>postchain-common</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>net.postchain</groupId>
                <artifactId>postchain-devtools</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>net.postchain</groupId>
                <artifactId>postchain-client</artifactId>
                <version>${project.version}</version>
            </dependency>

            <!-- Testing -->
            <dependency>
                <groupId>junit</groupId>
                <artifactId>junit</artifactId>
                <version>${junit.version}</version>
                <scope>test</scope>
            </dependency>
            <dependency>
                <groupId>pl.pragmatists</groupId>
                <artifactId>JUnitParams</artifactId>
                <version>${junitparams.version}</version>
                <scope>test</scope>
            </dependency>
            <dependency>
                <groupId>org.mockito.kotlin</groupId>
                <artifactId>mockito-kotlin</artifactId>
                <version>${mockito-kotlin.version}</version>
                <scope>test</scope>
            </dependency>
            <dependency>
                <groupId>org.mockito</groupId>
                <artifactId>mockito-core</artifactId>
                <version>${mockito.version}</version>
                <scope>test</scope>
            </dependency>
            <dependency>
                <groupId>com.willowtreeapps.assertk</groupId>
                <artifactId>assertk</artifactId>
                <version>${assertk.version}</version>
                <scope>test</scope>
            </dependency>
            <dependency>
                <groupId>org.skyscreamer</groupId>
                <artifactId>jsonassert</artifactId>
                <version>1.5.0</version>
                <scope>test</scope>
            </dependency>
            <dependency>
                <groupId>org.testcontainers</groupId>
                <artifactId>testcontainers</artifactId>
                <version>1.12.5</version>
                <scope>test</scope>
            </dependency>

            <!-- Json -->
            <dependency>
                <groupId>com.google.code.gson</groupId>
                <artifactId>gson</artifactId>
                <version>${gson.version}</version>
            </dependency>

            <!-- 3rd party -->
            <dependency>
                <groupId>io.rest-assured</groupId>
                <artifactId>rest-assured</artifactId>
                <version>3.1.1</version>
            </dependency>
            <dependency>
                <groupId>org.awaitility</groupId>
                <artifactId>awaitility</artifactId>
                <scope>test</scope>
                <version>${awaitility.version}</version>
            </dependency>
            <dependency>
                <groupId>org.awaitility</groupId>
                <artifactId>awaitility-kotlin</artifactId>
                <scope>test</scope>
                <version>${awaitility.version}</version>
            </dependency>
            <dependency>
                <groupId>com.github.ajalt</groupId>
                <artifactId>clikt</artifactId>
                <version>1.4.0</version>
            </dependency>
            <dependency>
                <groupId>org.apache.httpcomponents</groupId>
                <artifactId>httpclient</artifactId>
                <version>${httpclient.version}</version>
            </dependency>

        </dependencies>
    </dependencyManagement>

    <build>
        <sourceDirectory>src/main/kotlin</sourceDirectory>
        <testSourceDirectory>src/test/kotlin</testSourceDirectory>
        <plugins>
            <plugin>
                <artifactId>kotlin-maven-plugin</artifactId>
                <groupId>org.jetbrains.kotlin</groupId>
                <version>${kotlin.version}</version>

                <executions>
                    <execution>
                        <id>compile</id>
                        <goals>
                            <goal>compile</goal>
                        </goals>
                    </execution>

                    <execution>
                        <id>test-compile</id>
                        <goals>
                            <goal>test-compile</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>${maven.compiler.plugin.version}</version>
                <executions>
                    <!-- Replacing default-compile as it is treated specially by maven -->
                    <execution>
                        <id>default-compile</id>
                        <phase>none</phase>
                    </execution>
                    <!-- Replacing default-testCompile as it is treated specially by maven -->
                    <execution>
                        <id>default-testCompile</id>
                        <phase>none</phase>
                    </execution>
                    <execution>
                        <id>java-compile</id>
                        <phase>compile</phase>
                        <goals>
                            <goal>compile</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>java-test-compile</id>
                        <phase>test-compile</phase>
                        <goals>
                            <goal>testCompile</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <source>11</source>
                    <target>11</target>
                </configuration>
            </plugin>

            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>versions-maven-plugin</artifactId>
                <version>2.5</version>
            </plugin>

            <plugin>
                <groupId>pl.project13.maven</groupId>
                <artifactId>git-commit-id-plugin</artifactId>
                <version>4.0.0</version>
                <executions>
                    <execution>
                        <id>get-the-git-infos</id>
                        <goals>
                            <goal>revision</goal>
                        </goals>
                        <phase>initialize</phase>
                    </execution>
                </executions>
                <configuration>
                    <useNativeGit>true</useNativeGit> <!-- git worktree does not work with JGit implementation -->
                    <includeOnlyProperties>
                        <includeOnlyProperty>^git\.branch$</includeOnlyProperty>
                        <includeOnlyProperty>^git\.build\.(time|version)$</includeOnlyProperty>
                        <includeOnlyProperty>^git\.commit\.id\.(abbrev|full)$</includeOnlyProperty>
                        <includeOnlyProperty>^git\.commit\.message\.(full|short)$</includeOnlyProperty>
                        <includeOnlyProperty>^git\.commit\.time$</includeOnlyProperty>
                        <includeOnlyProperty>^git\.dirty$</includeOnlyProperty>
                    </includeOnlyProperties>
                    <commitIdGenerationMode>full</commitIdGenerationMode>
                    <dateFormatTimeZone>UTC</dateFormatTimeZone>
                </configuration>
            </plugin>

            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>properties-maven-plugin</artifactId>
                <version>1.0.0</version>
                <executions>
                    <execution>
                        <phase>generate-resources</phase>
                        <goals>
                            <goal>write-project-properties</goal>
                        </goals>
                        <configuration>
                            <outputFile>${project.build.outputDirectory}/${project.artifactId}-maven.properties
                            </outputFile>
                        </configuration>
                    </execution>
                </executions>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-dependency-plugin</artifactId>
                <version>3.1.1</version>
                <executions>
                    <execution>
                        <id>analyze</id>
                        <phase>generate-resources</phase>
                        <goals>
                            <goal>list</goal>
                        </goals>
                        <configuration>
                            <outputFile>${project.build.outputDirectory}/${project.artifactId}-dependencies.txt
                            </outputFile>
                            <sort>true</sort>
                        </configuration>
                    </execution>
                </executions>
            </plugin>

            <plugin>
                <groupId>external.atlassian.jgitflow</groupId>
                <artifactId>jgitflow-maven-plugin</artifactId>
                <version>${jgitflow.maven.plugin.version}</version>
                <configuration>
                    <enableSshAgent>true</enableSshAgent>
                    <autoVersionSubmodules>true</autoVersionSubmodules>
                    <allowUntracked>true</allowUntracked>
                    <pushFeatures>true</pushFeatures>
                    <pushReleases>true</pushReleases>
                    <pushHotfixes>true</pushHotfixes>
                    <noDeploy>true</noDeploy>
                    <flowInitContext>
                        <developBranchName>dev</developBranchName>
                        <versionTagPrefix>ver-</versionTagPrefix>
                    </flowInitContext>
                    <!-- Use these two lines to avoid entering of login/password manually -->
                    <username>${env.POSTCHAIN_REPOSITORY_USERNAME}</username>
                    <password>${env.POSTCHAIN_REPOSITORY_PASSWORD}</password>
                    <!-- Use these two lines to enter login/password manually -->
                    <!--
                    <username />
                    <password />
                    -->
                </configuration>
            </plugin>

        </plugins>
    </build>

    <profiles>
        <profile>
            <id>ci</id>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-surefire-plugin</artifactId>
                        <version>${maven.surefire.plugin.version}</version>
                        <configuration>
                            <includes>
                                <include>**/*Test.java</include>
                                <include>**/*IT.java</include>
                                <include>**/*Nightly.java</include>
                                <include>**/*E2ET.*</include>
                                <!--                                <include>**/FourPeersReconfigurationTest.java</include>-->
                            </includes>
                        </configuration>
                    </plugin>
                </plugins>
            </build>
        </profile>
    </profiles>

</project>
